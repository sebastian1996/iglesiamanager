<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login',function(){
	return view('login');
});

Route::get('/home',function(){
	return view('home');
});

Route::get('/Usuario/Insertar', function(){
	return view('Usuario.Insertar');
});

Route::get('/Usuario', function(){
	return view('Usuario.Listar');
});

Route::get('/Usuario/CambiarContraseña', function(){
	return view('Usuario.CambiarPassword');
});

Route::get('/Persona/Insertar', function(){
	return view('Persona.Insertar');
});

Route::get('/Persona', function(){
	return view('Persona.Listar');
});