<!DOCTYPE html>
<html>
<head>
  <title>Iglesia</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script type="text/javascript" src="js/validaciones.js"></script>
</head>

<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="home">Iglesia</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">

          <li class="dropdown" id="fun_usu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#" id="insertFun">Registrar Usuarios</a></li>
              <li><a href="#" id="listFun">Listar Usuarios</a></li>
            </ul>
          </li>

          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Personas <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#" id="insertPer">Registrar Personas</a></li>
              <li><a href="#" id="listPer">Listar Personas</a></li>
            </ul>
          </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#" id="change_pwd">Cambiar contraseña</a></li>
              <li><a href="#" id="exit">Salir</a></li>
            </ul>
          </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
  <div class="container" id="view">
    <h1 id="nombre_sesion"></h1>
  </div>
</body>

<script type="text/javascript">
  $(function() {
   $.ajax({url: "api/v1/sesion",type:"get", success: function(result){
    if(result.success){
      $('#nombre_sesion').html("Bienvenido "+result.data.nombre);
      if (result.data.rol != "1") {
        $('#fun_usu').css({'display':'none'});
      }
    }else{
      window.location = window.location.href.split('home')[0]+"login";
    };
  }});

   $('#exit').click(function() {
    $.ajax({url: "api/v1/sesion/logout",type:"get", success: function(result){
      if (result.success) {
        window.location = window.location.href.split('home')[0]+"login";
      }
    }});
  });

   $('#change_pwd').click(function() {
    changePwd();
  });

   $('#insertFun').click(function() {
    insertFun();
  });

   $('#listFun').click(function() {
    listFun();
  });

   $('#insertPer').click(function() {
    insertPer();
  });

   $('#listPer').click(function() {
    listPer();
  });
 });

  function changePwd() {
    $.ajax({url: "Usuario/CambiarContraseña",type:"get", success: function(result){
      $('#view').html(result);
    }});
  }

  function insertFun() {
    $.ajax({url: "Usuario/Insertar",type:"get", success: function(result){
      $('#view').html(result);
    }});
  }

  function listFun() {
    $.ajax({url: "Usuario",type:"get", success: function(result){
      $('#view').html(result);
    }});
  }

  function insertPer() {
    $.ajax({url: "Persona/Insertar",type:"get", success: function(result){
      $('#view').html(result);
    }});
  }

  function listPer() {
    $.ajax({url: "Persona",type:"get", success: function(result){
      $('#view').html(result);
    }});
  }
</script>

</html>