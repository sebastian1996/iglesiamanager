<style type="text/css">
table th, td{
	text-align: center;
}
</style>

<div class="table-responsive">
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Id</th>
				<th>Nombre</th>
				<th>Telefono</th>
				<th>Sexo</th>
				<th>Fecha nacimiento</th>
				<th>Editar</th>
				<th>Eliminar</th>
			</tr>  	
		</thead>
		<tbody>	
		</tbody>
	</table>
</div>

<div id="myModal2" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel">Modificar Usuario</h4>
			</div>
			<div class="modal-body">
				<form>
					<input type="hidden" id="id_upd">
					<div class="form-group">
						<label for="nombre_usu">Nombre completo</label>
						<input type="text" class="form-control" id="nombre_usu">
					</div>
					<div class="form-group">
						<label for="clave_usu">Telefono</label>
						<input type="text" class="form-control" id="clave_usu">
					</div>

					<div class="form-group">
						<label for="opt">Sexo</label>
						<select class="form-control" id="opt">
							<option value="M">Masculino</option>
							<option value="F">Femenino</option>
						</select>
					</div>

					<div class="form-group">
						<label for="date">Fecha nacimiento</label>
						<div class='input-group date' >
							<input type='date' class="form-control" id="date"/>
							<span class="input-group-addon">
								<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary" onclick="actualizar();">Actualizar</button>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	var token = $('#token').val();
	listar_personas();
	function listar_personas() {
		var html_ ="";
		$.ajax({url: "api/v1/Personas",type:"get", 
				success: function(result){					
					for (var i = 0; i < result.data.length; i++) {
						html_ += '<tr>'+
									'<td>'+result.data[i].id+'</td>'+
									'<td>'+result.data[i].nombre_completo+'</td>'+
									'<td>'+result.data[i].telefono+'</td>'+
									'<td>'+result.data[i].sexo+'</td>'+
									'<td>'+result.data[i].fecha_nacimiento+'</td>'+
									'<td>'+
										"<button type='button' class='btn btn-primary' onclick='editar("+JSON.stringify(result.data[i])+")'>"+
											'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>'+
										'</button>'+
									'</td>'+
									'<td>'+
										'<button type="button" class="btn btn-danger" onclick="eliminar('+result.data[i].id+')">'+
											'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'+
										'</button>'+
									'</td>'+
								'</tr>';
					}
					$('tbody').html(html_);
				},error(msg){
					alert('Error de disparidad');
				}
			});
		
	}

	function eliminar(argument) {
		if (confirm("¿Seguro?")) {
			$.ajax({url: "api/v1/Personas/"+argument+"/delete",headers:{'X-CSRF-TOKEN':token},data:{_method:'DELETE'},type:"post", 
				success: function(result){					
					listPer();
					alert(result.message);
				},error(msg){
					alert('Error de disparidad');
				}
			});
		}
	}

	function editar(argument) {		
		$('#nombre_usu').val(argument.nombre_completo);
		$('#clave_usu').val(argument.telefono);
		$('#opt').val(argument.sexo);
		$('#id_upd').val(argument.id);
		$('#date').val(argument.fecha_nacimiento);
		$('#myModal2').modal('show');
	}

	function actualizar() {
		if (nulos([$('#nombre_usu').val(), $('#clave_usu').val(), $('#date').val()])) {
			alert('Tiene un campo vacío');
		}else if(letras([$('#nombre_usu').val()])){
			alert('El nombre sólo debe tener letras');
		}else if(numeros_enteros([$('#clave_usu').val()])){
			alert('El teléfono sólo debe digitar números');
		}else if(fecha([$('#date').val()])){
			alert('La fecha debe ser menor a la actual');
		}else{
			var datos = {
				Persona:{
					nombre_completo:$('#nombre_usu').val(),
					telefono:$('#clave_usu').val(),
					sexo:$('#opt').val(),
					fecha_nacimiento:$('#date').val(),
				},
				_method:'PUT'
			};
			$.ajax({url: "api/v1/Personas/"+$('#id_upd').val()+"/edit",headers:{'X-CSRF-TOKEN':token}, data:datos, type:"post", 
				success: function(result){
					if(result.success){
						$("#myModal2").removeClass("in");
						$(".modal-backdrop").remove();
						$("#myModal2").hide();
						listPer();
					};
					alert(result.message);
				}
			});
		}
	}
</script>