<h1>Insertar Persona</h1>

<div class="form-group">
	<label for="nombre_usu">Nombre completo</label>
	<input type="text" class="form-control" id="nombre_usu">
</div>
<div class="form-group">
	<label for="clave_usu">Telefono</label>
	<input type="text" class="form-control" id="clave_usu">
</div>

<div class="form-group">
	<label for="opt">Sexo</label>
	<select class="form-control" id="opt">
		<option value="M">Masculino</option>
		<option value="F">Femenino</option>
	</select>
</div>

<div class="form-group">
	<label for="date">Fecha nacimiento</label>
	<div class='input-group date' >
		<input type='date' class="form-control" id="date"/>
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>
</div>

<button type="button" id="RegUsu" class="btn btn-default">Registrar</button>


<script type="text/javascript">
	$(function() {

		$('#RegUsu').click(function() {
			if (nulos([$('#nombre_usu').val(), $('#clave_usu').val(), $('#date').val()])) {
				alert('Tiene un campo vacío');
			}else if(letras([$('#nombre_usu').val()])){
				alert('El nombre sólo debe tener letras');
			}else if(numeros_enteros([$('#clave_usu').val()])){
				alert('El teléfono sólo debe digitar números');
			}else if(fecha([$('#date').val()])){
				alert('La fecha debe ser menor a la actual');
			}else{
				var token = $('#token').val();
				var datos = {
					persona:{
						nombre_completo:$('#nombre_usu').val(),
						telefono:$('#clave_usu').val(),
						sexo:$('#opt').val(),
						fecha_nacimiento:$('#date').val(),
						id_usuario:""
					}
				};	
				$.ajax({url: "api/v1/Personas",headers:{'X-CSRF-TOKEN':token},type:"post", data:datos, 
					success: function(result){
						console.log(result);
						alert(result.message);
						if (result.success) {
							insertPer();
						}
					}
				});
			}
			
		});
	});
</script>