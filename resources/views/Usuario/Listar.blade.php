<style type="text/css">
	table th, td{
		text-align: center;
	}
</style>
<div class="table-responsive">
	<table class="table table-bordered">
	  <thead>
	  	<tr>
	  		<th>Id</th>
	  		<th>Nombre</th>
	  		<th>Rol</th>
	  		<th>Editar</th>
	  		<th>Eliminar</th>
	  	</tr>  	
	  </thead>
	  <tbody>
	  </tbody>
	</table>
</div>

<div id="myModal" class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Modificar Usuario</h4>
      </div>
      <div class="modal-body">
        <form>
        	<input type="hidden" id="id_upd">
          	<div class="form-group">
				<label for="nombre_usu_upd">Nombre</label>
				<input type="text" class="form-control" id="nombre_usu_upd">
			</div>

			<div class="form-group">
				<label for="opt_upd">Rol</label>
				<select class="form-control" id="opt_upd">
				  <option value="1">Administrador</option>
				  <option value="2">Funcionario</option>
				</select>
			</div>

			 <div class="checkbox">
			    <label>
			      <input id="chkClave" type="checkbox" onchange="estado_input(this.checked)"> Cambiar Contraseña
			    </label>
			  </div>

			<div class="form-group">
				<label for="clave_usu_upd">Contraseña</label>
				<input type="password" class="form-control" id="clave_usu_upd" disabled="true">
			</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" onclick="actualizar();">Actualizar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	var token = $('#token').val();
	listar_usuarios();

	function listar_usuarios() {
		$.ajax({url: "api/v1/Usuarios",type:"get", 
				success: function(result){	
					var html_ = "";
					for (var i = 0; i < result.data.length; i++) {
						html_ += '<tr>'+
						  		'<td>'+result.data[i].id+'</td>'+
						  		'<td>'+result.data[i].user_name+'</td>'+
						  		'<td>'+result.data[i].nombre+'</td>'+
						  		'<td>'+
						  			'<button type="button" class="btn btn-primary" onclick=editar('+JSON.stringify(result.data[i])+')>'+
						  				'<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>'+
						  			'</button>'+
						  		'</td>'+
						  		'<td>'+
						  			'<button type="button" class="btn btn-danger" onclick="eliminar('+result.data[i].id+')">'+
						  				'<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>'+
						  			'</button>'+
						  		'</td>'+
						  	'</tr>';
					}
					$('tbody').html(html_);
					
				},error(msg){
					alert('Error de disparidad');
				}
			});
	}

	function eliminar(argument) {
		if (confirm("¿Seguro?")) {
			$.ajax({url: "api/v1/Usuarios/"+argument+"/delete",headers:{'X-CSRF-TOKEN':token},data:{_method:'DELETE'},type:"post", 
				success: function(result){					
					listFun();
					alert(result.message);
				},error(msg){
					alert('Error de disparidad');
				}
			});
		}
	}

	function editar(argument) {
		$('#nombre_usu_upd').val(argument.user_name);
		$('#opt_upd').val(argument.rol_id);
		$('#id_upd').val(argument.id);
		$('#myModal').modal('show');
	}

	function estado_input(argument) {
		$( "#clave_usu_upd" ).prop( "disabled", !argument);
	}

	function actualizar() {
		var datos = {
			Usuario:{
				user_name:$('#nombre_usu_upd').val(),				
				id_rol:$('#opt_upd').val(),
			},
			_method:'PUT'
		};

		if ($('#chkClave').prop('checked')) {
			var array = [$('#nombre_usu_upd').val(), $('#clave_usu_upd').val()];
		}else{
			var array = [$('#nombre_usu_upd').val()];
		}

		if (nulos(array)) {
			alert('Tiene un campo nulo');
		}else{
			if ($('#chkClave').prop('checked')) {
				$.ajax({url: "api/v1/Usuarios/Restablecer/"+$('#id_upd').val(),headers:{'X-CSRF-TOKEN':token}, data:{Usuario:{password:$('#clave_usu_upd').val()}}, type:"post", 
					success: function(result){
						console.log(result);
					}
				});
			}

			$.ajax({url: "api/v1/Usuarios/"+$('#id_upd').val()+"/edit",headers:{'X-CSRF-TOKEN':token}, data:datos, type:"post", 
				success: function(result){
					if(result.success){
						$("#myModal").removeClass("in");
	    				$(".modal-backdrop").remove();
	    				$("#myModal").hide();
						listFun();
					};
					alert(result.message);
				}
			});
		}
	}
</script>