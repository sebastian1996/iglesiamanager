<h1>Insertar Usuario</h1>

<div class="form-group">
	<label for="nombre_usu">Nombre</label>
	<input type="text" class="form-control" id="nombre_usu">
</div>

<div class="form-group">
	<label for="clave_usu">Contraseña</label>
	<input type="password" class="form-control" id="clave_usu">
</div>

<div class="form-group">
	<label for="clave_usu">Repita contraseña</label>
	<input type="password" class="form-control" id="clave_usu_rep">
</div>

<div class="form-group">
	<label for="opt">Rol</label>
	<select class="form-control" id="opt">
		<option value="1">Administrador</option>
		<option value="2">Funcionario</option>
	</select>
</div>

<button type="button" id="RegUsu" class="btn btn-default">Registrar</button>

<script type="text/javascript">
	$(function() {
		$('#RegUsu').click(function() {
			if (nulos([$('#nombre_usu').val(), $('#clave_usu').val(),
			$('#clave_usu_rep').val()])) {
				alert('Tiene un campo vacío');
			}else if(claves_iguales([$('#clave_usu').val(), $('#clave_usu_rep').val()])){
				alert('Las contraseñas no son iguales');
			}else{
				var token = $('#token').val();
				var datos = {
					usuario:{
						user_name:$('#nombre_usu').val(),
						password:$('#clave_usu').val(),
						activo:1,
						id_rol:$('#opt').val()}
					};
					$.ajax({url: "api/v1/Usuarios",
						headers:{'X-CSRF-TOKEN':token},
						type:"post",
						data:datos, 
						success: function(result){
							alert(result.message);
							if (result.success) {
								insertFun();
							}
						}
					});
			}
		});
	});
</script>