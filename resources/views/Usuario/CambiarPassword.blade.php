<h1>Cambiar Password</h1>

<div class="form-group">
	<label for="clave_actual">Contraseña Actual</label>
	<input type="password" class="form-control" id="clave_actual">
</div>

<div class="form-group">
	<label for="clave_nueva">Nueva Contraseña</label>
	<input type="password" class="form-control" id="clave_nueva">
</div>

<div class="form-group">
	<label for="clave_confirmar">Confirme Nueva Contraseña</label>
	<input type="password" class="form-control" id="clave_confirmar">
</div>

<button type="button" id="RegCambio" class="btn btn-default">Enviar</button>

<script type="text/javascript">
	$(function() {
		var token = $('#token').val();
		$("#RegCambio").click(function() {
			if (nulos([$('#clave_actual').val(), $('#clave_nueva').val(),
			$('#clave_confirmar').val()])) {
				alert('Tiene un campo nulo');
			}else if(claves_iguales([$('#clave_nueva').val(), $('#clave_confirmar').val()])){
				alert('Las contraseñas no son iguales');
			}else{
				var datos = {
					usuario:{
						password:$('#clave_actual').val(),
						password_new:$('#clave_nueva').val(),
						confirm_new:$('#clave_confirmar').val()
					}
				};

				$.ajax({url: "api/v1/Usuarios/CambiarClave",headers:{'X-CSRF-TOKEN':token},type:"post", data:datos, 
					success: function(result){
						alert(result.message);
						if (result.success) {
							changePwd();
						}
					}
				});
			}
		});
	});
</script>