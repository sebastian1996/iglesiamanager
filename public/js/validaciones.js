function nulos(arguments) {
	for (var i = 0; i < arguments.length; i++) {
		if(arguments[i] === "" || arguments[i] === undefined || arguments[i] == null || arguments[i].length <= 0){
			return true;
			break;
		}
	}

	return false;
};

function letras(arguments) {
	for (var i = 0; i < arguments.length; i++) {               
		if(!/^[a-zA-ZáéíóúÁÉÍÓÚñÑ\s]*$/.test(arguments[i])) {			
			return true;
			break;
		}
	}

	return false;
}

function numeros_enteros(arguments) {
	for (var i = 0; i < arguments.length; i++) {               
		if(!/^[0-9]*$/.test(arguments[i])) {
			return true;
			break;
		}
	}

	return false;
}

function correo(argument) {
	if(!/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$/.test(argument)) {
		return true;
	}

	return false;
}

function claves_iguales(arguments) {
	return !(arguments[0] === arguments[1]);
}

function fecha(argument) {
	var actual = new Date();
	var enviado = new Date(argument);
	if (enviado.getFullYear() > actual.getFullYear() || 
		(enviado.getFullYear() == actual.getFullYear() && (enviado.getMonth()+1) > (actual.getMonth()+1)) ||
		(enviado.getFullYear() == actual.getFullYear() && (enviado.getMonth()+1) == (actual.getMonth()+1) 
			&& enviado.getDate() >= actual.getDate()) ) {
			return true;		
	}

	return false;
}