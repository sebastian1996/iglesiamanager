<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project\IglesiaManager;

use App\Models\Usuario;

class SesionController extends IglesiaManagerController
{

    public static $NAME = "SesionController";

    public function index()
    {        
        if(!IglesiaManager::validar_session()){
            return $this->response(false, "ERROR: No ha iniciado sesión");
        }
        
        return $this->response(true, null, IglesiaManager::get_session_usuario());
    }

    public function validar(Request $request)
    {
    	$usuario = Usuario::Buscar_by_Credenciales($request['usuario'], $request['contraseña']);
    	
    	if(!$usuario){
    		return $this->response(false, "ERROR: Credenciales incorrectas");
    	}        

        $datos = ['id'=>$usuario->id,'nombre'=>$usuario->user_name,'rol'=>$usuario->id_rol];

        IglesiaManager::set_session(IglesiaManager::$SESSION, $datos);

    	return $this->response(true ,"Sesión iniciada correctamente", $datos);
    }

    public function salir()
    {        
        IglesiaManager::destroy_session_usuario();

        return $this->response(true, "Sesión cerrada correctamente");
    }
}