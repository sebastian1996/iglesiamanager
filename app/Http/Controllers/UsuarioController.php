<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project\IglesiaManager;

use App\Models\Usuario;
use App\Models\Rol;

class UsuarioController extends IglesiaManagerController
{

    public static $NAME = "UsuarioController";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->response(true, null, Usuario::Listar());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request->all();
        $usuario_id = Usuario::insertGetId($datos['usuario']);
        $usuario = Usuario::find($usuario_id)->first();

        return $this->response(true, "Registrado correctamente", $usuario);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $data = $request['usuario'];
        $id = IglesiaManager::get_session_usuario()['id'];
        $model = Usuario::find($id);
        if ($model->password == $data['password']) {
            $model->password = $data['password_new'];
            $model->save();
            return $this->response(true, "Contraseña cambiada correctamente");
        } else {
            return $this->response(false, "ERROR: Contraseña incorrecta", null);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usuario_data = $request['Usuario'];
        $usuario = Usuario::find($id);

        $usuario->user_name = $usuario_data['user_name'];
        $usuario->id_rol = $usuario_data['id_rol'];

        $usuario->save();

        return $this->response(true, "Actualizado correctamente", $usuario);
    }

    public function restore(Request $request, $id)
    {
        if (IglesiaManager::get_session_usuario()['id'] != "1") {
            return $this->response(false, "ERROR: No tiene permisos para esta petición");
        }

        $usuario = Usuario::find($id);

        $usuario->password = $request['Usuario']['password'];

        $usuario->save();

        return $this->response(true, "Actualizado correctamente");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::find($id);
        $usuario->delete();

        return $this->response(true, "Eliminado correctamente");
    }
}
