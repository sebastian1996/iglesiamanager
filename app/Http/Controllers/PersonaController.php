<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project\IglesiaManager;

use App\Models\Persona;

class PersonaController extends IglesiaManagerController
{

    public static $NAME = "PersonaController";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (IglesiaManager::get_session_usuario()['id'] == "1") {
            return $this->response(true, null, Persona::all());
        } else {
            return $this->response(true, null, Persona::get_by_user($id));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $datos = $request['persona'];

        $datos['id_usuario'] = IglesiaManager::get_session_usuario()['id'];

        $persona_id = Persona::insertGetId($datos);
        $persona = Persona::find($persona_id)->first();

        return $this->response(true, "Registrado Correctamente", $persona);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $persona_data = $request->all();
        $persona = Persona::find($id);

        $persona->nombre_completo = $persona_data['Persona']['nombre_completo'];
        $persona->telefono = $persona_data['Persona']['telefono'];
        $persona->sexo = $persona_data['Persona']['sexo'];
        $persona->fecha_nacimiento = $persona_data['Persona']['fecha_nacimiento'];

        $persona->save();

        return $this->response(true, "Actualizado Correctamente", $persona);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $persona = Persona::find($id);
        $persona->delete();

        return $this->response(true, "Eliminado Correctamente");
    }
}
