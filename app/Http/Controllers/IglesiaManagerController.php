<?php

namespace App\Http\Controllers;

use Illuminate\Database\QueryException;
use DB;
use Log;
use Closure;

class IglesiaManagerController extends Controller {

    // Constructor de la clase WorkShopController
    
    public function __construct() { 
        session_start(); 
    }
    
    // Métodos de la clase WorkShopController

    public function response($success, $message, $data = null, $code = null) {
        return [
            "success" => $success, "message" => $message, "data" => $data, "code" => $code
        ];
    }

    // Métodos de control DAO

    public function transaction(Closure $callback, Closure $exception = null) {
        DB::beginTransaction(); // Iniciando transacción
        
        try {
            $result = $callback(); DB::commit();
        } catch (QueryException $ex) {
            DB::rollback(); Log::error($ex->getMessage());
            
            $result = ($exception == null) ? 
                $this->response(false, $ex->getMessage(), $ex) :
                $exception($ex); // Exception
        }
        
        return $result; // Resultado de la acción ejecutada
    }
}