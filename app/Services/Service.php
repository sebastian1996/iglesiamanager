<?php

namespace App\Services;

use Route;

class Service {
    
    // Atributos de la clase Service
    
    public static $HTTP_GET         = 1;
    public static $HTTP_POST        = 2;
    public static $HTTP_PUT         = 3;
    public static $HTTP_DELETE      = 4;
    public static $HTTP_RESOURCE    = 5;

    // Métodos de la clase Service
    
    private static function define_service($method, $route, $controller, $function) {
        switch ($method) {
            case (Service::$HTTP_GET) :
                return Route::get($route, $controller."@".$function);

            case (Service::$HTTP_POST) :
                return Route::post($route, $controller."@".$function);

            case (Service::$HTTP_PUT) :
                return Route::put($route, $controller."@".$function);

            case (Service::$HTTP_DELETE) :
                return Route::delete($route, $controller."@".$function);

            case (Service::$HTTP_RESOURCE) :
                return Route::resource($route, $controller."@".$function);
        }
    }

    public static function set_services($services, $middlewares = null) {
        foreach ($services as $service) {
            $controller = $service["controller"]; // Controlador
            $method = $service["method"]; // Método HTTP
            $route = $service["route"]; // Ruta del servicio
            $function = $service["function"]; // Función a invocar

            $route_http = (new static)->define_service(
                $method, $route, $controller, $function
            );
            
            if ($middlewares != null) {
                $route_http->middleware($middlewares);
            } // Definiendo Middlewares

            if (isset($service["where"])) {
                $route_http->where(
                    $service["where"]->name(), $service["where"]->filter()
                );
            } // Se ha definido filtro para el servicio
        }
    }
}