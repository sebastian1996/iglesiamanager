<?php

namespace App\Services\Session;

use App\Project\IglesiaManager;

class Basic {
    
    // Métodos de la clase Basic
    
    public static function Sesion() {
        return include IglesiaManager::get_route_resources()."/Session/Basic/Sesion.php";
    }
    
    public static function Persona() {
        return include IglesiaManager::get_route_resources()."/Session/Basic/Persona.php";
    }
    
    public static function Usuario() {
        return include IglesiaManager::get_route_resources()."/Session/Basic/Usuario.php";
    }
}