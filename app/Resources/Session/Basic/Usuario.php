<?php

use App\Services\Service;
use App\Http\Controllers\UsuarioController;

return [
    [ 
        "method" => Service::$HTTP_GET,
        "route" => "/Usuarios",
        "function" => "index",
        "controller" => UsuarioController::$NAME
    ],[ 
        "method" => Service::$HTTP_POST,
        "route" => "/Usuarios",
        "function" => "store",
        "controller" => UsuarioController::$NAME
    ],[ 
        "method" => Service::$HTTP_PUT,
        "route" => "/Usuarios/{id}/edit",
        "function" => "update",
        "controller" => UsuarioController::$NAME
    ],[ 
        "method" => Service::$HTTP_POST,
        "route" => "/Usuarios/CambiarClave",
        "function" => "edit",
        "controller" => UsuarioController::$NAME
    ],[ 
        "method" => Service::$HTTP_POST,
        "route" => "/Usuarios/Restablecer/{id}",
        "function" => "restore",
        "controller" => UsuarioController::$NAME
    ], [ 
        "method" => Service::$HTTP_DELETE,
        "route" => "/Usuarios/{id}/delete",
        "function" => "destroy",
        "controller" => UsuarioController::$NAME
    ]
];