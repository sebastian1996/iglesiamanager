<?php

use App\Services\Service;
use App\Http\Controllers\PersonaController;

return [
    [ 
        "method" => Service::$HTTP_GET,
        "route" => "/Personas",
        "function" => "index",
        "controller" => PersonaController::$NAME
    ], [ 
        "method" => Service::$HTTP_POST,
        "route" => "/Personas",
        "function" => "store",
        "controller" => PersonaController::$NAME
    ], [ 
        "method" => Service::$HTTP_PUT,
        "route" => "/Personas/{id}/edit",
        "function" => "update",
        "controller" => PersonaController::$NAME
    ], [ 
        "method" => Service::$HTTP_DELETE,
        "route" => "/Personas/{id}/delete",
        "function" => "destroy",
        "controller" => PersonaController::$NAME
    ]
];