<?php

use App\Services\Service;
use App\Http\Controllers\SesionController;

return [
    [ 
        "method" => Service::$HTTP_GET,
        "route" => "/sesion",
        "function" => "index",
        "controller" => SesionController::$NAME
    ], [ 
        "method" => Service::$HTTP_POST,
        "route" => "/sesion",
        "function" => "validar",
        "controller" => SesionController::$NAME
    ], [ 
        "method" => Service::$HTTP_GET,
        "route" => "sesion/logout",
        "function" => "salir",
        "controller" => SesionController::$NAME
    ]
];