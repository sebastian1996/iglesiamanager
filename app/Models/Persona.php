<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
	protected $table = "personas";

    const CREATED_AT = 'fechahora_registro';
	const UPDATED_AT = 'fechahora_actualizacion';

	public static function get_by_user($id_user) {
		return Persona::where('id_usuario', $id_user)->get();
	}
}
