<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "usuarios";

    const CREATED_AT = 'fechahora_registro';
	const UPDATED_AT = 'fechahora_actualizacion';

    public static function Buscar_by_Credenciales($usuario, $clave)
    {
        return Usuario::where('user_name', $usuario)
                      ->where('password', $clave)
                      ->first();
    }

    public static function Listar(){
    	return Usuario::join('roles','usuarios.id_rol','roles.id')->
    					select('usuarios.id','usuarios.user_name','roles.id as rol_id','roles.nombre')->get();
    }
}
