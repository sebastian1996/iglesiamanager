<?php

namespace App\Project;

use Carbon\Carbon;

class IglesiaManager {
    
    // Atributos de PitstopManager
    
    public static $SESSION = "iglesia";

    // Métodos para gestión de sesión
    
    public static function set_session($key, $value) {
        $_SESSION[$key] = $value;
    }

    public static function validar_session()
    {
        return isset($_SESSION[IglesiaManager::$SESSION]);
    }

    public static function get_session_usuario() {
        return $_SESSION[IglesiaManager::$SESSION];
    }
    
    public static function get_session($key) {
        return $_SESSION[$key];
    }

    public static function destroy_session_usuario() {
        unset($_SESSION[IglesiaManager::$SESSION]);
    }
    
    public static function get_route_resources() {
        return "../app/Resources";
    }

    // Métodos avanzados de la aplicación
    
    public static function set_key_value(&$object, $data, $key) {
        if (isset($data[$key])) {
            $object[$key] = $data[$key]; return true;
        } // Se ha definido valor a cambiar
                
        return false; // No se ha definido valor a cambiar
    }
    
    public static function set_date_value(&$object, $data, $key) {
        if (isset($data[$key])) {
            $date = Carbon::createFromTimestamp($data[$key])->toDateString();
            
            $object[$key] = $date; return true;
        } // Se ha definido valor a cambiar
                
        return false; // No se ha definido valor a cambiar
    }
    
    public static function date_of_timestamp($timestamp) {
        return Carbon::createFromTimestamp($timestamp)->toDateString();
    }
}