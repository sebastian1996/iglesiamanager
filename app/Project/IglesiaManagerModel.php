<?php

namespace App\Project;

use Illuminate\Database\Eloquent\Model;

class PitstopManagerModel extends Model {
    
    // Atributos de la clase PitstopManagerModel
    
    const CREATED_AT = "fechahora_registro";
    const UPDATED_AT = "fechahora_actualizacion";

    public $timestamps = false;
    
    // Métodos de la clase PitstopManagerModel
    
    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
    }
}